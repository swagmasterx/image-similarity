#include <iostream>
#include <experimental/filesystem>
#include "opencv2/core.hpp"
#include "opencv2/imgcodecs.hpp"
#include "opencv2/imgproc.hpp"

namespace fs = std::experimental::filesystem;

int main(int argc, char** argv)
{
	constexpr double threshold{ 1.0 };

	const fs::path test_path{ [=]() {
		return argc >= 2 ? argv[1] : "test";
	}() };

	const fs::path res_path{ [=]() {
		return argc >= 3 ? argv[2] : "res";
	}() };

	const fs::path src_path{ [=]() {
		return argc >= 4 ? argv[3] : "source";
	}() };

	fs::remove_all(res_path);
	fs::create_directory(res_path);

	for (const auto &src_entry : fs::recursive_directory_iterator{ src_path }) {
		std::cout << "============================================\n";
		std::cout << src_entry << "\n";
		std::cout << "--------------------------------------------\n";
		const auto src_img{ cv::imread(src_entry.path().string(), cv::IMREAD_GRAYSCALE) };

		if (!src_img.data) {
			continue;
		}

		const auto cur_res_path{ fs::path{ res_path }.append(src_entry.path().filename().replace_extension()) };

		fs::create_directory(cur_res_path);

		const int hist_size{ 256 };
		const float range[]{ 0.f, 256.f };
		const float* hist_range = { range };
		const bool uniform{ true };
		const bool accumulate{ false };

		cv::Mat src_hist;
		calcHist(&src_img, 1, 0, cv::Mat(), src_hist, 1, &hist_size, &hist_range, uniform, accumulate);

		for (const auto &test_entry : fs::recursive_directory_iterator{ test_path }) {
			std::cout << test_entry << "\n";

			cv::Mat test_img = cv::imread(test_entry.path().string(), cv::IMREAD_GRAYSCALE);
			if (!test_img.data) {
				continue;
			}

			cv::Mat test_hist;
			calcHist(&test_img, 1, 0, cv::Mat(), test_hist, 1, &hist_size, &hist_range, uniform, accumulate);

			const auto histDiff{ compareHist(src_hist, test_hist, cv::HISTCMP_BHATTACHARYYA) };

			cv::Mat temp_match_res;
			matchTemplate(src_hist, test_hist, temp_match_res, cv::TM_CCOEFF_NORMED);
			const auto tempDiff{ 1 - temp_match_res.at<float>({0, 0}) };

			const auto adjDiff{ (histDiff / 10) + tempDiff };

			if (adjDiff < threshold) {
				std::cout << "-- Diff: " << adjDiff << "\n";
				fs::copy(test_entry.path(), { fs::path{cur_res_path}.append(test_entry.path().filename()) });
			}
			std::cout << "\n";
		}
	}

	getchar();
}